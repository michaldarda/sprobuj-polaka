var CommandExecutor = {
  ENTER: 13,

  start: function(){
    $('#expression').keypress(this.submitEnterKey);
    $("#expressionForm").submit(this.execute);
  },

  submitEnterKey: function(e){
    if(e.which == this.ENTER) {
        $('#expressionForm').submit;
    }
  },

  execute: function(e){
    e.preventDefault();
    command = $('#expression').val();
    $('#expression').val('');

    $.ajax({
      type: "POST",
      url: "/",
      data: { command: command },
      success: function(e){
        $('#consoleOutput').append('<p>' + e + '</p>')
      },
      failure: function(e){ console.log(e) }
    })}
};

$(document).ready(function(){
  CommandExecutor.start();
});

